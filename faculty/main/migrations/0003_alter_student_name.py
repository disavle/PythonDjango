# Generated by Django 4.0.4 on 2022-04-21 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_alter_faculty_options_alter_speciality_options_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Имя'),
        ),
    ]
