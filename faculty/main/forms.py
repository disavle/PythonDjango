from .models import Faculty, Student, Speciality
from django.forms import ModelForm, TextInput, ModelChoiceField, Select


class FacultyAddForm(ModelForm):
    class Meta:
        model = Faculty
        fields = ["name"]
        widgets = {"name": TextInput(attrs={
            'class': 'form-control',
            'placeholder': "Имя",
        'aria-label': "Name",
        'aria-describedby': "basic-addon1",
        'required': ""
        })}

class FacultyEditForm(ModelForm):
    class Meta:
        model = Faculty
        fields = ["name"]
        widgets = {"name": TextInput(attrs={
            'class': 'form-control',
            'placeholder': "Имя",
        'aria-label': "Name",
        'aria-describedby': "basic-addon1",
        'required': ""
        })}

class SpecialtyAddForm(ModelForm):
    class Meta:
        model = Speciality
        fields = ["name"]
        widgets = {"name": TextInput(attrs={
            'class': 'form-control',
            'placeholder': "Имя",
            'aria-label': "Name",
            'aria-describedby': "basic-addon1",
            'required': ""
        })}

class SpecialityEditForm(ModelForm):
    class Meta:
        model = Faculty
        fields = ["name"]
        widgets = {"name": TextInput(attrs={
            'class': 'form-control',
            'placeholder': "Имя",
        'aria-label': "Name",
        'aria-describedby': "basic-addon1",
        'required': ""
        })}

class StudentAddForm(ModelForm):
    facultyId = ModelChoiceField(queryset=Faculty.objects.all(), empty_label="Выберите факультет", to_field_name="name", widget = Select(attrs={'class': 'form-select'}))
    specialityId = ModelChoiceField(queryset=Speciality.objects.all(), empty_label="Выберите специальность",to_field_name="name", widget = Select(attrs={'class': 'form-select'}))
    class Meta:

        model = Student
        fields = ["name", "dateIn", "facultyId", "specialityId", "course", "group"]
        widgets = {"name": TextInput(attrs={
            'class': 'form-control',
            'placeholder': "Имя",
            'aria-label': "Name",
            'aria-describedby': "basic-addon1",
            'required': ""
        }),
            "dateIn": TextInput(attrs={
                'class': 'form-control',
                'pattern': "\d{4}-\d{2}-\d{2}",
                'placeholder': "2019-09-02",
                'aria-label': "Name",
                'aria-describedby': "basic-addon1",
                'required': ""
            }),
            "course": TextInput(attrs={
                'class': 'form-control',
                'pattern': "[0-9]+",
                'placeholder': "3",
                'aria-label': "Name",
                'aria-describedby': "basic-addon1",
                'required': ""
            }),
            "group": TextInput(attrs={
                'class': 'form-control',
                'pattern': "[0-9]+",
                'placeholder': "37",
                'aria-label': "Name",
                'aria-describedby': "basic-addon1",
                'required': ""
            })

        }

class StudentEditForm(ModelForm):
    facultyId = ModelChoiceField(queryset=Faculty.objects.all(), empty_label="Выберите факультет", widget = Select(attrs={'class': 'form-select'}))
    specialityId = ModelChoiceField(queryset=Speciality.objects.all(), empty_label="Выберите специальность", widget = Select(attrs={'class': 'form-select'}))

    class Meta:

        model = Student
        fields = ["name", "dateIn", "facultyId", "specialityId", "course", "group"]
        widgets = {"name": TextInput(attrs={
            'class': 'form-control',
            'placeholder': "Имя",
            'aria-label': "Name",
            'aria-describedby': "basic-addon1",
            'required': ""
        }),
            "dateIn": TextInput(attrs={
                'class': 'form-control',
                'pattern': "\d{4}-\d{2}-\d{2}",
                'placeholder': "2019-09-02",
                'aria-label': "Name",
                'aria-describedby': "basic-addon1",
                'required': ""
            }),
            "course": TextInput(attrs={
                'class': 'form-control',
                'pattern': "[0-9]+",
                'placeholder': "3",
                'aria-label': "Name",
                'aria-describedby': "basic-addon1",
                'required': ""
            }),
            "group": TextInput(attrs={
                'class': 'form-control',
                'pattern': "[0-9]+",
                'placeholder': "37",
                'aria-label': "Name",
                'aria-describedby': "basic-addon1",
                'required': ""
            })

        }