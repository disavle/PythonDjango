from django.urls import path
from . import views


urlpatterns = [
    path('',views.index, name = "home"),
    path('faculty',views.faculty, name = "faculty"),
    path('student',views.student, name = "student"),
    path('speciality',views.speciality, name = "speciality"),
    path('faculty/edit/<int:id>',views.facultyEdit, name = "facultyEdit"),
    path('faculty/delete/<int:id>/', views.facultyDelete, name="facultyDelete"),
    path('student/edit/<int:id>', views.studentEdit, name="studentEdit"),
path('student/delete/<int:id>/', views.studentDelete, name="studentDelete"),
    path('speciality/edit/<int:id>', views.specialityEdit, name="specialityEdit"),
path('speciality/delete/<int:id>/', views.specialityDelete, name="specialityDelete"),
]
