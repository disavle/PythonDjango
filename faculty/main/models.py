from django.db import models


class Faculty(models.Model):
    name = models.CharField('Наименование', max_length=100)
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Факультет"
        verbose_name_plural = "Факультеты"


class Speciality(models.Model):
    name = models.CharField('Наименование', max_length=100)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = "Специальность"
        verbose_name_plural = "Специальности"

class Student(models.Model):
    name = models.CharField('Имя', max_length=100)
    dateIn = models.DateField('Дата поступления')
    facultyId = models.ForeignKey(Faculty, on_delete=models.CASCADE)
    specialityId = models.ForeignKey(Speciality, on_delete=models.CASCADE)
    course = models.CharField('Курс', max_length=100)
    group = models.CharField('Группа', max_length=100)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"
