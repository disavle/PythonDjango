from django.shortcuts import render, redirect, get_object_or_404
from .models import Faculty, Speciality, Student
from .forms import *


def index(request):
    return render(request, 'main/main.html')

def faculty(request):
    if request.method == 'POST':
        form = FacultyAddForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("faculty")
    form = FacultyAddForm()
    facultys = Faculty.objects.order_by('id')
    context = {
        'form': form,
        'facultys': facultys
    }
    return render(request, 'main/faculty.html', context)

def facultyEdit(request, id):
    instance = get_object_or_404(Faculty, id=id)
    if request.method == 'POST':
        form = FacultyEditForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return redirect("faculty")
    facultyEdited = Faculty.objects.get(id=id)
    facultys = Faculty.objects.order_by('id')
    form = FacultyEditForm(instance=facultyEdited)
    context = {
        'form': form,
        'facultys': facultys,
        'id': id
    }
    return render(request, 'main/faculty.html', context)

def facultyDelete(request, id):
    instance = get_object_or_404(Faculty, id=id)
    instance.delete()
    return redirect("faculty")


def student(request):
    if request.method == 'POST':
        form = StudentAddForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("student")
    form = StudentAddForm()
    students = Student.objects.order_by('id')
    context = {
        'form': form,
        'students': students
    }
    return render(request, 'main/student.html', context)

def studentEdit(request, id):
    instance = get_object_or_404(Student, id=id)
    if request.method == 'POST':
        form = StudentEditForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return redirect("student")
    studentEdited = Student.objects.get(id=id)
    students = Student.objects.order_by('id')
    form = StudentEditForm(instance=studentEdited)
    context = {
        'form': form,
        'students': students,
        'id': id
    }
    return render(request, 'main/student.html', context)

def studentDelete(request, id):
    instance = get_object_or_404(Student, id=id)
    instance.delete()
    return redirect("student")

def speciality(request):
    if request.method == 'POST':
        form = SpecialtyAddForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("speciality")
    form = SpecialtyAddForm()
    specialitys = Speciality.objects.order_by('id')
    context = {
        'form': form,
        'specialitys': specialitys,
    }
    return render(request, 'main/speciality.html', context)

def specialityEdit(request, id):
    instance = get_object_or_404(Speciality, id=id)
    if request.method == 'POST':
        form = SpecialityEditForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return redirect("speciality")
    specialityEdited = Speciality.objects.get(id=id)
    specialitys = Speciality.objects.order_by('id')
    form = SpecialityEditForm(instance=specialityEdited)
    context = {
        'form': form,
        'specialitys': specialitys,
        'id': id
    }
    return render(request, 'main/speciality.html', context)

def specialityDelete(request, id):
    instance = get_object_or_404(Speciality, id=id)
    instance.delete()
    return redirect("speciality")